﻿USE [SVC_AUTENTICACAO];

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PreCadastro]') AND type in (N'U'))

  BEGIN

    CREATE TABLE [dbo].[PreCadastro](
      [Identificador] [uniqueidentifier] NOT NULL,
      [CodigoPais] [char](2) NOT NULL,
      [NumeroIdentificacao] [varchar](20) NOT NULL,
      [Nome] [varchar](100) NOT NULL,
      [NomeSocial] [varchar](100),
      [DataNascimento] [datetime] NOT NULL,
      [Email] [varchar](100) NOT NULL,
      [DataCriacao] [datetime] NOT NULL,
      [DataAlteracao] [datetime] NULL,
    PRIMARY KEY CLUSTERED ([Identificador] ASC) ON [PRIMARY])

    ALTER TABLE [dbo].[PreCadastro] ADD  DEFAULT (newid()) FOR [Identificador]

  END
