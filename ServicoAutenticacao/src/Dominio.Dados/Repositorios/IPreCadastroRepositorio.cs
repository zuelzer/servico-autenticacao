﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Dominio.Dados.Repositorios
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dominio.Modelos.Entidades;
    using System;
    using System.Threading.Tasks;

    public interface IPreCadastroRepositorio
    {
        Task<PreCadastro> Obter(Guid identificador);

        Task<PreCadastro> ObterPorChaveComposta(string codigoPais, string numeroIdentificacao);

        Task<PreCadastro> Inserir(PreCadastro preCadastro);

        Task<PreCadastro> Atualizar(PreCadastro preCadastro);

        Task Deletar(PreCadastro preCadastro);
    }
}
