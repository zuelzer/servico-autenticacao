﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Apresentacao.API.Configuration
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Configuration;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Apresentacao.API.Extensions;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.OpenApi.Models;

    internal static class SwaggerSetup
    {
        private static SwaggerConfiguration swaggerConfiguration;

        internal static IServiceCollection AddSwaggerEnabled(this IServiceCollection services, SwaggerConfiguration configuration)
        {
            swaggerConfiguration = configuration;

            if (swaggerConfiguration.Enabled)
            {
                services.AddSwaggerGen(c =>
                {
                    c.CustomSchemaIds(x => x.FullName);
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Serviço de Autenticação", Version = "v1" });
                    c.RegisterDocumentation();
                });
            }

            return services;
        }

        internal static IApplicationBuilder UseSwaggerEnabled(this IApplicationBuilder app)
        {
            if (swaggerConfiguration.Enabled)
            {
                app.UseSwagger();

                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint($"/swagger/v1/swagger.json", "Serviço de Autenticação");
                    c.EnableValidator();
                });
            }

            return app;
        }
    }
}
