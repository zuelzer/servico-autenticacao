﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Apresentacao.API.Extensions
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Apresentacao.API.RouteConfigs;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;

    public static class MvcOptionsExtensions
    {
        public static void UseCentralRoutePrefix(this MvcOptions opts, IRouteTemplateProvider routeAttribute)
            => opts.Conventions.Insert(0, new RouteConvention(routeAttribute));
    }
}
