﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Apresentacao.API.Extensions
{
    using Microsoft.Extensions.Configuration;

    public static class ConfigurationExtensions
    {
        public static T Load<T>(this IConfiguration configuration, string sectionName)
            where T : new()
        {
            T config = new T();
            configuration.GetSection(sectionName).Bind(config);

            return config;
        }
    }
}
