﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Apresentacao.API.Extensions
{
    using Microsoft.Extensions.DependencyInjection;
    using Swashbuckle.AspNetCore.SwaggerGen;
    using System.IO;

    public static class SwaggerConfigExtensions
    {
        public static void RegisterDocumentation(this SwaggerGenOptions swaggerGenOptions)
        {
            var presentationXmlDocumentFile = $"{System.AppDomain.CurrentDomain.FriendlyName}.xml";
            var presentationXmlDocFullFilePath = Path.Combine(System.AppContext.BaseDirectory, presentationXmlDocumentFile);

            RegisterXmlCommentsIfFileExists(swaggerGenOptions, presentationXmlDocFullFilePath);

            var applicationDTOXmlDocumentFile = "Employer.Plataforma.Interno.ServiceAutenticacao.Aplicacao.DTO.XML";
            var applicationDTOXmlDocFullFilePath = Path.Combine(System.AppContext.BaseDirectory, applicationDTOXmlDocumentFile);

            RegisterXmlCommentsIfFileExists(swaggerGenOptions, applicationDTOXmlDocFullFilePath);
        }

        private static void RegisterXmlCommentsIfFileExists(SwaggerGenOptions swaggerGenOptions, string filePath)
        {
            if (File.Exists(filePath))
            {
                swaggerGenOptions.IncludeXmlComments(filePath);
            }
        }
    }
}
