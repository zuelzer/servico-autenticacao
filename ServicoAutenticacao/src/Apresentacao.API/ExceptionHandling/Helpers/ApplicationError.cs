﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Apresentacao.API.ExceptionHandling.Helpers
{
    using System.Net;

    public class ApplicationError
    {
        public ApplicationError() { }

        public ApplicationError(HttpStatusCode statusCode, string message)
        {
            this.StatusCode = statusCode;
            this.Message = message;
        }

        public HttpStatusCode StatusCode { get; set; }

        public string Message { get; set; }
    }
}
