﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Apresentacao.API.ExceptionHandling.Helpers
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Exceptions;
    using System;
    using System.Collections.Generic;
    using System.Net;

    public static class HttpStatusCodeHelper
    {
        private static readonly Dictionary<Type, HttpStatusCode> ExceptionDictionary = new Dictionary<Type, HttpStatusCode>
        {
            { typeof(ArgumentException), HttpStatusCode.BadRequest },
            { typeof(ArgumentOutOfRangeException), HttpStatusCode.BadRequest },
            { typeof(ArgumentNullException), HttpStatusCode.BadRequest },
            { typeof(NotImplementedException), HttpStatusCode.NotImplemented },
            { typeof(UnauthorizedAccessException), HttpStatusCode.Unauthorized },
            { typeof(InvalidDomainEntityException), HttpStatusCode.BadRequest },
            { typeof(InvalidDomainValueObjectException), HttpStatusCode.BadRequest },
            { typeof(ResourceNotFoundException), HttpStatusCode.NotFound },
            { typeof(ResourceAlreadyExistsException), HttpStatusCode.Conflict }
        };

        public static HttpStatusCode GetHttpStatusForException(Exception exception)
        {
            if (ExceptionDictionary.ContainsKey(exception.GetType()))
            {
                return ExceptionDictionary[exception.GetType()];
            }

            return HttpStatusCode.InternalServerError;
        }
    }
}
