namespace Employer.Plataforma.Interno.ServiceAutenticacao.Apresentacao.API
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Aplicacao.Servicos.AutoMapeamento;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Apresentacao.API.Configuration;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Apresentacao.API.ExceptionHandling;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Apresentacao.API.Extensions;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Apresentacao.Inicializacao;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dados.Repositorio.AutoMapeamento;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dados.Repositorio.Contexto;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Configuration;
    using global::AutoMapper;
    using HealthChecks.UI.Client;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Diagnostics.HealthChecks;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Formatters;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using Serilog;
    using System.Linq;

    public class Startup
    {
        public Startup(IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("conf/appsettings.json", optional: false, reloadOnChange: false)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var entityFrameworkConfiguration = this.Configuration.GetConfiguration<EntityFrameworkConfiguration>();
            var healthCheckConfiguration = this.Configuration.GetConfiguration<HealthCheckConfiguration>();
            var loggingConfiguration = this.Configuration.GetConfiguration<LoggingConfiguration>();
            var swaggerConfiguration = this.Configuration.GetConfiguration<SwaggerConfiguration>();

            InjetorNativo.RegisterServices(services, entityFrameworkConfiguration, healthCheckConfiguration, loggingConfiguration, swaggerConfiguration);

            services
                .AddSingleton(this.Configuration)
                .AddOptions()
                .AddSwaggerEnabled(swaggerConfiguration);

            services.AddMvc(opt =>
            {
                opt.UseCentralRoutePrefix(new RouteAttribute("Autenticacao/v1"));

                opt.EnableEndpointRouting = false;

                var noContentFormatter = opt.OutputFormatters.OfType<HttpNoContentOutputFormatter>().FirstOrDefault();

                if (noContentFormatter != null)
                {
                    noContentFormatter.TreatNullValueAsNoContent = false;
                }
            });

            services.AddHealthChecks()
                .AddDbContextCheck<SqlServerContext>("Infrastructure Dependency - SQL Server");
                //.AddUrlGroup(new Uri(userServiceConfiguration.BaseUrl), name: $"Service Dependency : User Service: {userServiceConfiguration.BaseUrl}", failureStatus: HealthStatus.Degraded)
                //.AddUrlGroup(new Uri(loggingConfiguration.ApplicationInsightUrl), name: $"Monitoring Dependency : Seq Logging: {loggingConfiguration.ApplicationInsightUrl}", failureStatus: HealthStatus.Degraded);

            services.AddHealthChecksUI()
                .AddInMemoryStorage();

            services.AddAutoMapper(typeof(PerfilMapeamentoDominioParaAplicacao), typeof(PerfilMapeamentoAplicacaoParaDominio), typeof(PerfilMapeamentoDominioParaDados), typeof(PerfilMapeamentoDadosParaDominio));

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, HealthCheckConfiguration healthCheckConfiguration)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHealthChecks(healthCheckConfiguration.DefaultPath, new HealthCheckOptions
            {
                Predicate = _ => true,
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            });

            app.UseHealthChecksUI(options =>
            {
                options.UIPath = healthCheckConfiguration.UIPath;
                options.ApiPath = healthCheckConfiguration.ApiPath;
            });

            app.UseExceptionHandler(a => a.Run(async context => await CustomExceptionHandler.HandleExceptionAsync(loggerFactory, context)));

            app.UseSwaggerEnabled();

            app.UseSerilogRequestLogging();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "default",
                    "api/{controller}/{id?}");
            });
        }
    }
}
