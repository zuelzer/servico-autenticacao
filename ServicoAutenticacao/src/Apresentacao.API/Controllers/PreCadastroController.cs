﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Apresentacao.API.Controllers
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Aplicacao.DTO.Solicitacao;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Aplicacao.Servicos.Interfaces;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Net;
    using System.Threading.Tasks;

    [ApiController]
    public class PreCadastroController : ControllerBase
    {
        private readonly IPreCadastroServicoAplicacao _preCadastroServicoAplicacao;

        public PreCadastroController(IPreCadastroServicoAplicacao preCadastroServicoAplicacao)
        {
            this._preCadastroServicoAplicacao = preCadastroServicoAplicacao;
        }

        /// <summary>
        /// Obter pré cadastro
        /// </summary>
        /// <param name="identificador"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("PreCadastro/{identificador:guid}", Name = "ObterPreCadastro")]
        [ProducesResponseType(typeof(Aplicacao.DTO.Resposta.PreCadastro), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> ObterPreCadastro([FromRoute] Guid identificador)
        {
            var result = await this._preCadastroServicoAplicacao.ObterPreCadastro(identificador).ConfigureAwait(false);

            return this.Ok(result);
        }

        /// <summary>
        /// Inserir pré cadastro
        /// </summary>
        /// <param name="preCadastro"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("PreCadastro", Name = "InserirPreCadastro")]
        [ProducesResponseType(typeof(Aplicacao.DTO.Resposta.PreCadastro), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> InserirPreCadastro([FromBody] PreCadastro preCadastro)
        {
            var result = await this._preCadastroServicoAplicacao.InserirPreCadastro(preCadastro).ConfigureAwait(false);

            return this.CreatedAtRoute("ObterPreCadastro", new { identificador = result.Identificador }, result);
        }

        /// <summary>
        /// Atualizar pré cadastro
        /// </summary>
        /// <param name="identificador"></param>
        /// <param name="preCadastro"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("PreCadastro/{identificador:guid}", Name = "AtualizarPreCadastro")]
        [ProducesResponseType(typeof(Aplicacao.DTO.Resposta.PreCadastro), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> AtualizarPreCadastro([FromRoute] Guid identificador, [FromBody] PreCadastro preCadastro)
        {
            var result = await this._preCadastroServicoAplicacao.AtualizarPreCadastro(identificador, preCadastro).ConfigureAwait(false);

            return this.Ok(result);
        }

        /// <summary>
        /// Deletar pré cadastro
        /// </summary>
        /// <param name="identificador"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("PreCadastro/{identificador:guid}", Name = "DeletarPreCadastro")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> DeletarPreCadastro([FromRoute] Guid identificador)
        {
            await this._preCadastroServicoAplicacao.DeletarPreCadastro(identificador).ConfigureAwait(false);

            return this.NoContent();
        }
    }
}
