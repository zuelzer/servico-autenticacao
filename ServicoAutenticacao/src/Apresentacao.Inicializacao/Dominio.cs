﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Apresentacao.Inicializacao
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dominio.Servicos.Implementacoes;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dominio.Servicos.Interfaces;
    using global::AutoMapper;
    using Microsoft.Extensions.DependencyInjection;

    public class Dominio
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped(sp => new Mapper(sp.GetRequiredService<IConfigurationProvider>(), sp.GetService));
            services.AddScoped<IPreCadastroServico, PreCadastroServico>();
        }
    }
}
