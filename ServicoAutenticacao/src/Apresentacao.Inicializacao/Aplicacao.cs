﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Apresentacao.Inicializacao
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Aplicacao.Servicos.Implementacoes;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Aplicacao.Servicos.Interfaces;
    using Microsoft.Extensions.DependencyInjection;

    public class Aplicacao
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IPreCadastroServicoAplicacao, PreCadastroServicoAplicacao>();
        }
    }
}
