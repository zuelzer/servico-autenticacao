﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Apresentacao.Inicializacao
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Configuration;
    using global::AutoMapper;
    using Microsoft.Extensions.DependencyInjection;

    public static class InjetorNativo
    {
        public static void RegisterServices(IServiceCollection services,
            EntityFrameworkConfiguration entityFrameworkConfiguration,
            HealthCheckConfiguration healthCheckConfiguration,
            LoggingConfiguration loggingConfiguration,
            SwaggerConfiguration swaggerConfiguration)
        {
            services.AddScoped(sp => new Mapper(sp.GetRequiredService<IConfigurationProvider>(), sp.GetService));

            Configuracao.RegisterServices(services, entityFrameworkConfiguration, healthCheckConfiguration, loggingConfiguration, swaggerConfiguration);
            Aplicacao.RegisterServices(services);
            Dominio.RegisterServices(services);
            Dados.RegisterServices(services, entityFrameworkConfiguration);
        }
    }
}
