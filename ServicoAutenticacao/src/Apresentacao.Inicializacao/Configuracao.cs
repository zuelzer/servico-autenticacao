﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Apresentacao.Inicializacao
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class Configuracao
    {
        public static void RegisterServices(IServiceCollection services,
            EntityFrameworkConfiguration entityFrameworkConfiguration,
            HealthCheckConfiguration healthCheckConfiguration,
            LoggingConfiguration loggingConfiguration,
            SwaggerConfiguration swaggerConfiguration)
        {
            services.AddSingleton<EntityFrameworkConfiguration>(entityFrameworkConfiguration);
            services.AddSingleton<HealthCheckConfiguration>(healthCheckConfiguration);
            services.AddSingleton<LoggingConfiguration>(loggingConfiguration);
            services.AddSingleton<SwaggerConfiguration>(swaggerConfiguration);
        }
    }
}
