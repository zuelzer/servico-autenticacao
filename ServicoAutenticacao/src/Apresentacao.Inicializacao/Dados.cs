﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Apresentacao.Inicializacao
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dados.Repositorio.Contexto;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dados.Repositorio.Implementacoes;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dominio.Dados.Repositorios;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using System;

    public class Dados
    {
        public static void RegisterServices(IServiceCollection services, EntityFrameworkConfiguration entityFrameworkConfiguration)
        {
            services.AddScoped<IPreCadastroRepositorio, PreCadastroRepositorio>();

            services.AddDbContext<SqlServerContext>();
            services.AddTransient<Func<SqlServerContext>>((provider) => new Func<SqlServerContext>(() => new SqlServerContext(entityFrameworkConfiguration)));
        }
    }
}
