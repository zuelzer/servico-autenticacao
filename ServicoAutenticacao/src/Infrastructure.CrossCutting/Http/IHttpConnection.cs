﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Http
{
    using System;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;

    public interface IHttpConnection
    {
        Task<TResponse> GetAsync<TResponse>(Uri uri, params HttpHeader[] headers);

        Task<TResponse> GetAsync<TResponse>(Uri uri, CancellationToken cancellationToken, params HttpHeader[] headers);

        Task<TResponse> PostAsync<TBody, TResponse>(Uri uri, TBody body, params HttpHeader[] headers);

        Task<TResponse> PostAsync<TBody, TResponse>(Uri uri, TBody body, CancellationToken cancellationToken, params HttpHeader[] headers);

        Task<TResponse> PutAsync<TBody, TResponse>(Uri uri, TBody body, params HttpHeader[] headers);

        Task<TResponse> PutAsync<TBody, TResponse>(Uri uri, TBody body, CancellationToken cancellationToken, params HttpHeader[] headers);

        Task<TResponse> PatchAsync<TBody, TResponse>(Uri uri, TBody body, params HttpHeader[] headers);

        Task<TResponse> PatchAsync<TBody, TResponse>(Uri uri, TBody body, CancellationToken cancellationToken, params HttpHeader[] headers);

        Task DeleteAsync(Uri uri, params HttpHeader[] headers);

        Task DeleteAsync(Uri uri, CancellationToken cancellationToken, params HttpHeader[] headers);

        Task HeadAsync(Uri uri, params HttpHeader[] headers);

        Task HeadAsync(Uri uri, CancellationToken cancellationToken, params HttpHeader[] headers);

        Task<HttpResponseMessage> ExecuteAsync(HttpMethod method, Uri uri, object body, HttpHeader[] headers, CancellationToken cancellationToken);
    }

}
