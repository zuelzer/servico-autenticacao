﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Http
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Serializer;
    using System;
    using System.Linq;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;

    public class HttpClientConnection : IHttpConnection
    {
        protected HttpClient httpClient;

        private readonly ISerializer serializer;
        private readonly string mediaType;

        private readonly string[] supportedMediaTypes =
        {
            "application/json-patch+json",
            "application/json",
            "application/octet-stream",
            "application/xml",
            "text/html",
            "text/plain"
        };

        public HttpClientConnection(HttpClient httpClient)
            : this(httpClient, new NewtonsoftSerializer(), null, "application/json")
        {
        }

        public HttpClientConnection(HttpClient httpClient, string userAgent)
            : this(httpClient, new NewtonsoftSerializer(), userAgent, "application/json")
        {
        }

        public HttpClientConnection(HttpClient httpClient, ISerializer serializer, string userAgent, string mediaType)
        {
            if (!supportedMediaTypes.Contains(mediaType))
            {
                throw new ArgumentException("Unsupported media type.", nameof(mediaType));
            }

            this.httpClient = httpClient;
            this.serializer = serializer;
            this.mediaType = mediaType;

            this.AddUserAgent(userAgent);
            this.ValidateUserAgents();
        }

        public Task<TResponse> GetAsync<TResponse>(Uri uri, params HttpHeader[] headers)
        {
            return this.GetAsync<TResponse>(uri, CancellationToken.None, headers);
        }

        public async Task<TResponse> GetAsync<TResponse>(Uri uri, CancellationToken cancellationToken, params HttpHeader[] headers)
        {
            using (HttpResponseMessage response = await this.ExecuteAsync(HttpMethod.Get, uri, null, headers, cancellationToken).ConfigureAwait(false))
            {
                return await this.ProcessResponse<TResponse>(response).ConfigureAwait(false);
            }
        }

        public Task<TResponse> PostAsync<TBody, TResponse>(Uri uri, TBody body, params HttpHeader[] headers)
        {
            return this.PostAsync<TBody, TResponse>(uri, body, CancellationToken.None, headers);
        }

        public async Task<TResponse> PostAsync<TBody, TResponse>(Uri uri, TBody body, CancellationToken cancellationToken, params HttpHeader[] headers)
        {
            using (HttpResponseMessage response = await this.ExecuteAsync(HttpMethod.Post, uri, body, headers, cancellationToken).ConfigureAwait(false))
            {
                return await this.ProcessResponse<TResponse>(response).ConfigureAwait(false);
            }
        }

        public Task<TResponse> PutAsync<TBody, TResponse>(Uri uri, TBody body, params HttpHeader[] headers)
        {
            return this.PutAsync<TBody, TResponse>(uri, body, CancellationToken.None, headers);
        }

        public async Task<TResponse> PutAsync<TBody, TResponse>(Uri uri, TBody body, CancellationToken cancellationToken, params HttpHeader[] headers)
        {
            using (HttpResponseMessage response = await this.ExecuteAsync(HttpMethod.Put, uri, body, headers, cancellationToken).ConfigureAwait(false))
            {
                return await this.ProcessResponse<TResponse>(response).ConfigureAwait(false);
            }
        }

        public Task<TResponse> PatchAsync<TBody, TResponse>(Uri uri, TBody body, params HttpHeader[] headers)
        {
            return this.PatchAsync<TBody, TResponse>(uri, body, CancellationToken.None, headers);
        }

        public async Task<TResponse> PatchAsync<TBody, TResponse>(Uri uri, TBody body, CancellationToken cancellationToken, params HttpHeader[] headers)
        {
            using (HttpResponseMessage response = await this.ExecuteAsync(new HttpMethod("PATCH"), uri, body, headers, cancellationToken).ConfigureAwait(false))
            {
                return await this.ProcessResponse<TResponse>(response).ConfigureAwait(false);
            }
        }

        public Task DeleteAsync(Uri uri, params HttpHeader[] headers)
        {
            return this.DeleteAsync(uri, CancellationToken.None, headers);
        }

        public async Task DeleteAsync(Uri uri, CancellationToken cancellationToken, params HttpHeader[] headers)
        {
            await this.ExecuteAsync(HttpMethod.Delete, uri, null, headers, cancellationToken).ConfigureAwait(false);
        }

        public Task HeadAsync(Uri uri, params HttpHeader[] headers)
        {
            return this.HeadAsync(uri, CancellationToken.None, headers);
        }

        public async Task HeadAsync(Uri uri, CancellationToken cancellationToken, params HttpHeader[] headers)
        {
            await this.ExecuteAsync(HttpMethod.Head, uri, null, headers, cancellationToken).ConfigureAwait(false);
        }

        public async Task<HttpResponseMessage> ExecuteAsync(HttpMethod method, Uri uri, object body, HttpHeader[] headers, CancellationToken cancellationToken)
        {
            if (this.httpClient.BaseAddress != null && !uri.IsAbsoluteUri)
            {
                uri = new Uri(this.httpClient.BaseAddress, uri);
            }

            var message = new HttpRequestMessage(method, uri);

            if (headers != null)
            {
                foreach (HttpHeader header in headers)
                {
                    message.Headers.Add(header.Name, header.Value);
                }
            }

            if (body != null)
            {
                if (body is HttpContent)
                {
                    message.Content = (HttpContent)body;
                }
                else
                {
                    message.Content = new StringContent(this.serializer.Encoding.GetString(this.serializer.Serialize(body)), this.serializer.Encoding, this.mediaType);
                }
            }

            var addHeadersTask = this.AddHeaders(message, cancellationToken);

            if (addHeadersTask != null)
            {
                await addHeadersTask.ConfigureAwait(false);
            }

            try
            {
                return await this.httpClient.SendAsync(message, HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected virtual Task AddHeaders(HttpRequestMessage message, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        private async Task<TResponse> ProcessResponse<TResponse>(HttpResponseMessage response)
        {
            byte[] content = null;

            if (response.Content != null)
            {
                content = await response.Content.ReadAsByteArrayAsync().ConfigureAwait(false);
            }

            return content != null && content.Length > 0 ? this.serializer.Deserialize<TResponse>(content) : default(TResponse);
        }

        private void AddUserAgent(string userAgent)
        {
            if (!string.IsNullOrEmpty(userAgent) && !this.httpClient.DefaultRequestHeaders.UserAgent.Any(x => x.ToString() == userAgent))
            {
                if (!this.httpClient.DefaultRequestHeaders.UserAgent.TryParseAdd(userAgent))
                {
                    throw new FormatException("Invalid User-Agent");
                }
            }
        }

        private void ValidateUserAgents()
        {
            if (this.httpClient.DefaultRequestHeaders.UserAgent.Count == 0)
            {
                throw new Exception("HttpClient doesn't have an User-Agent");
            }
        }
    }
}
