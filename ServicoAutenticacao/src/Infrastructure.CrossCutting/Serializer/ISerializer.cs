﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Serializer
{
    using System.Text;

    public interface ISerializer
    {
        string Name { get; }

        string MediaType { get; }

        Encoding Encoding { get; }

        byte[] Serialize(object item);

        T Deserialize<T>(byte[] serializedObject);
    }
}
