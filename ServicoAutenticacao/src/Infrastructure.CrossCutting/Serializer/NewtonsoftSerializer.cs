﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Serializer
{
    using Newtonsoft.Json;
    using System.Text;

    public class NewtonsoftSerializer : ISerializer, IStringSerializer
    {
        private readonly JsonSerializerSettings settings;

        public NewtonsoftSerializer(JsonSerializerSettings settings)
        {
            this.settings = settings ?? new JsonSerializerSettings();
        }

        public NewtonsoftSerializer()
        {
            this.settings = new JsonSerializerSettings();
        }

        public string Name => "Newtonsoft";

        public Encoding Encoding => Encoding.UTF8;

        public string MediaType => "application/json";

        public byte[] Serialize(object item)
        {
            var jsonString = this.SerializeToString(item);
            return Encoding.GetBytes(jsonString);
        }

        public T Deserialize<T>(byte[] serializedObject)
        {
            var jsonString = Encoding.GetString(serializedObject);
            return JsonConvert.DeserializeObject<T>(jsonString, this.settings);
        }

        public string SerializeToString(object item)
        {
            return JsonConvert.SerializeObject(item, typeof(object), this.settings);
        }
    }
}
