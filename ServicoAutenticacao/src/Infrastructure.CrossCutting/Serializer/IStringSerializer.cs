﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Serializer
{
    public interface IStringSerializer
    {
        string SerializeToString(object item);
    }
}
