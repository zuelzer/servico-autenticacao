﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Configuration
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Exceptions;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Extensions;
    using Microsoft.Extensions.Configuration;
    using System;
    using System.Collections.Generic;

    public static class ConfigurationProvider
    {
        private static readonly Dictionary<Type, string> ConfigurationKeys = new Dictionary<Type, string>
        {
            { typeof(EntityFrameworkConfiguration), "EntityFrameworkConfiguration" },
            { typeof(HealthCheckConfiguration), "HealthCheckConfiguration" },
            { typeof(LoggingConfiguration), "LoggingConfiguration" },
            { typeof(SwaggerConfiguration), "SwaggerConfiguration" }
        };

        public static T GetConfiguration<T>(this IConfiguration configuration) where T : class
        {
            if (!ConfigurationKeys.TryGetValue(typeof(T), out string key))
            {
                Intercept.Against<ArgumentNullException>(string.IsNullOrEmpty(key), "Configuration class does not exist");
            }

            return configuration.GetSection(key).Get<T>();
        }

        public static T GetConfigurationValue<T>(string value, T defaultValue)
        {
            return GetConfigurationValue(value, defaultValue, false);
        }

        private static T GetConfigurationValue<T>(string value, T defaultValue, bool throwException)
        {
            if (value.IsNull())
            {
                return defaultValue;
            }

            try
            {
                if (typeof(Enum).IsAssignableFrom(typeof(T)))
                {
                    return (T)Enum.Parse(typeof(T), value);
                }

                return value.To<T>();
            }
            catch (Exception ex)
            {
                if (throwException)
                {
                    throw ex;
                }

                return defaultValue;
            }
        }
    }
}
