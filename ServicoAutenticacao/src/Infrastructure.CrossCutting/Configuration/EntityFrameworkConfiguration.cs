﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Configuration
{
    using System.Text;

    public class EntityFrameworkConfiguration
    {
        public string DataSource { get; set; }

        public string InitialCatalog { get; set; }
        
        public bool PersistSecurityInfo { get; set; }
        
        public string IntegratedSecurity { get; set; }
        
        public string UserID { get; set; }
        
        public string Password { get; set; }
        
        public string App { get; set; }
        
        public bool MultipleActiveResultSets { get; set; }
        
        public bool Encrypt { get; set; }
        
        public bool TrustServerCertificate { get; set; }
        
        public int ConnectionTimeout { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.Append($"Data Source={DataSource};");
            sb.Append($"Initial Catalog={InitialCatalog};");
            sb.Append($"Persist Security Info={PersistSecurityInfo.ToString()};");

            if (!string.IsNullOrEmpty(IntegratedSecurity))
                sb.Append($"Integrated Security={IntegratedSecurity};");

            if (!string.IsNullOrEmpty(UserID))
                sb.Append($"User ID={UserID};");

            if (!string.IsNullOrEmpty(Password))
                sb.Append($"Password={Password};");

            if (!string.IsNullOrEmpty(App))
                sb.Append($"App={App};");

            sb.Append($"MultipleActiveResultSets={MultipleActiveResultSets.ToString()};");
            sb.Append($"Encrypt={Encrypt.ToString()};");
            sb.Append($"TrustServerCertificate={TrustServerCertificate.ToString()};");
            sb.Append($"Connection Timeout={ConnectionTimeout.ToString()};");

            return sb.ToString();
        }
    }
}
