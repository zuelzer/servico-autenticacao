﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Extensions
{
    using System;
    using System.Reflection;

    public static class PropertyInfoExtensions
    {
        private static readonly Func<PropertyInfo, object, object> PropertyGetter;

        static PropertyInfoExtensions()
        {
            PropertyGetter = CreateOpenInstancePropertyGetter();
        }

        public static object FastGetValue(this PropertyInfo propInfo, object obj)
        {
            return PropertyGetter(propInfo, obj);
        }

        private static Func<PropertyInfo, object, object> CreateOpenInstancePropertyGetter()
        {
            var methodInfo = typeof(PropertyInfo).GetMethod("GetValue", new[] { typeof(object) });

            return Delegate.CreateDelegate(typeof(Func<PropertyInfo, object, object>), methodInfo) as Func<PropertyInfo, object, object>;
        }
    }
}