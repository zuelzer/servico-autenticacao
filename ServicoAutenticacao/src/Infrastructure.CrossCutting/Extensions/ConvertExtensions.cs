﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Extensions
{
    using System;

    public static class ConvertExtensions
    {
        public static T To<T>(this object obj)
        {
            Type type = typeof(T);

            if (type.IsGenericType && (type.GetGenericTypeDefinition() == typeof(Nullable<>)))
            {
                if (obj == null)
                {
                    return (T)(object)null;
                }

                return (T)Convert.ChangeType(obj, Nullable.GetUnderlyingType(type));
            }

            if (typeof(Enum).IsAssignableFrom(typeof(T)))
            {
                return (T)Enum.ToObject(typeof(T), obj);
            }

            return (T)Convert.ChangeType(obj, type);
        }
    }
}
