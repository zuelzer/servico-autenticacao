﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Extensions
{
    using System.Text.RegularExpressions;

    public static class StringExtensions
    {
        public static bool IsNumber(this string str)
        {
            Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");

            return regex.IsMatch(str);
        }

        public static bool IsNumberWithWhiteSpace(this string str)
        {
            return IsNumber(str.Replace(" ", string.Empty));
        }

        public static bool IsNotNullOrEmpty(this string str)
        {
            return !str.IsNullOrEmpty();
        }

        public static bool IsNullOrEmpty(this string str)
        {
            return str == null || str.Length == 0;
        }

        public static bool IsNullOrWhiteSpace(this string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }
    }
}
