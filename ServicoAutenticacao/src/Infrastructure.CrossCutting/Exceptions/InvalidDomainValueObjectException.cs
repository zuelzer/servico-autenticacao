﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Exceptions
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class InvalidDomainValueObjectException : Exception
    {
        public InvalidDomainValueObjectException()
        {
        }

        public InvalidDomainValueObjectException(string message)
            : base(message)
        {
        }

        public InvalidDomainValueObjectException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
