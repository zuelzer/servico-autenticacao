﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Exceptions
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class InvalidDomainEntityException : Exception
    {
        public InvalidDomainEntityException()
        {
        }

        public InvalidDomainEntityException(string message)
            : base(message)
        {
            this.Message = message;
        }

        public InvalidDomainEntityException(string message, Exception inner)
            : base(message, inner)
        {
            this.Message = message;
        }

        public new string Message { get; set; }
    }
}
