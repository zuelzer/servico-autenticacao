﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Exceptions
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Extensions;
    using Newtonsoft.Json;
    using System;

    public static class Intercept
    {
        public static void Against<TException>(bool assertion, string message)
            where TException : Exception
        {
            Against<TException>(assertion, message, logData: null, innerException: null);
        }

        public static void Against<TException>(bool assertion, string message, object logData)
            where TException : Exception
        {
            Against<TException>(assertion, message, logData, innerException: null);
        }

        public static void Against<TException>(bool assertion, string message, Exception innerException)
            where TException : Exception
        {
            Against<TException>(assertion, message, null, innerException);
        }

        public static void Against<TException>(bool assertion, string message, object logData, Exception innerException)
            where TException : Exception
        {
            Against(typeof(TException), assertion, message, logData, innerException);
        }

        public static void Against(Type exceptionType, bool assertion, string message, object logData, Exception innerException)
        {
            if (!assertion)
            {
                return;
            }

            if (!typeof(Exception).IsAssignableFrom(exceptionType))
            {
                throw new ArgumentException("exceptionType must inherit from Exception");
            }

            var exception = (Exception)exceptionType.GetConstructor(new[] { typeof(string), typeof(Exception) }).Invoke(new object[] { message, innerException });

            if (logData.IsNotNull())
            {
                exception.Data.Add("LogData", JsonConvert.SerializeObject(logData));
            }

            throw exception;
        }
    }
}
