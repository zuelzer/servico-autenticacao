﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Aplicacao.DTO.Solicitacao
{
    using System;

    public class PreCadastro
    {
        public string CodigoPais { get; set; }

        public string NumeroIdentificacao { get; set; }

        public string Nome { get; set; }

        public string NomeSocial { get; set; }

        public DateTime DataNascimento { get; set; }

        public string Email { get; set; }
    }
}
