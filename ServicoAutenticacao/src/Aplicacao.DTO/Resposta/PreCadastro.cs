﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Aplicacao.DTO.Resposta
{
    using System;

    public class PreCadastro
    {
        public Guid Identificador { get; set; }

        public string CodigoPais { get; set; }

        public string NumeroIdentificacao { get; set; }

        public string Nome { get; set; }

        public string NomeSocial { get; set; }

        public DateTime DataNascimento { get; set; }

        public string Email { get; set; }

        public DateTime DataCriacao { get; set; }

        public DateTime DataAlteracao { get; set; }
    }
}
