﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Dominio.Servicos.Interfaces
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dominio.Modelos.Entidades;
    using System;
    using System.Threading.Tasks;

    public interface IPreCadastroServico
    {
        Task<PreCadastro> ObterPreCadastro(Guid identificador);

        Task<PreCadastro> InserirPreCadastro(PreCadastro preCadastro);

        Task<PreCadastro> AtualizarPreCadastro(PreCadastro preCadastro);

        Task DeletarPreCadastro(Guid identificador);
    }
}
