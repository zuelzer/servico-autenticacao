﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Dominio.Servicos.Implementacoes
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dominio.Dados.Repositorios;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dominio.Modelos.Entidades;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dominio.Servicos.Interfaces;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Exceptions;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Extensions;
    using System;
    using System.Threading.Tasks;

    public class PreCadastroServico : IPreCadastroServico
    {
        private readonly IPreCadastroRepositorio _preCadastroRepositorio;

        public PreCadastroServico(IPreCadastroRepositorio preCadastroRepositorio)
        {
            this._preCadastroRepositorio = preCadastroRepositorio;
        }

        public async Task<PreCadastro> ObterPreCadastro(Guid identificador)
        {
            return await this._preCadastroRepositorio.Obter(identificador).ConfigureAwait(false);
        }

        public async Task<PreCadastro> InserirPreCadastro(PreCadastro preCadastro)
        {
            Intercept.Against<InvalidDomainEntityException>(preCadastro.IsNull(), "Pré cadastro não pode ser nulo");

            var preCadastroExistente = await this._preCadastroRepositorio.ObterPorChaveComposta(preCadastro.CodigoPais, preCadastro.NumeroIdentificacao).ConfigureAwait(false);
            Intercept.Against<ResourceAlreadyExistsException>(preCadastroExistente.IsNotNull(), "Pré cadastro já existe");

            preCadastro.Validar();

            return await this._preCadastroRepositorio.Inserir(preCadastro).ConfigureAwait(false);
        }

        public async Task<PreCadastro> AtualizarPreCadastro(PreCadastro preCadastro)
        {
            Intercept.Against<InvalidDomainEntityException>(preCadastro.IsNull(), "Pré cadastro não pode ser nulo");

            var preCadastroExistente = await this._preCadastroRepositorio.Obter(preCadastro.Identificador).ConfigureAwait(false);
            Intercept.Against<ResourceNotFoundException>(preCadastroExistente.IsNull(), "Pré cadastro não encontrado");

            preCadastro.Validar();

            return await this._preCadastroRepositorio.Atualizar(preCadastro).ConfigureAwait(false);
        }

        public async Task DeletarPreCadastro(Guid identificador)
        {
            var preCadastro = await this._preCadastroRepositorio.Obter(identificador).ConfigureAwait(false);

            Intercept.Against<ResourceNotFoundException>(preCadastro.IsNull(), "Pré cadastro não encontrado");

            await this._preCadastroRepositorio.Deletar(preCadastro).ConfigureAwait(false);
        }
    }
}
