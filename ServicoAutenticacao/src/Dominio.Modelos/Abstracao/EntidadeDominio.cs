﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Dominio.Modelos.Abstracao
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Extensions;
    using System;

    public abstract class EntidadeDominio
    {
        protected EntidadeDominio()
        {
            this.Identificador = Guid.NewGuid();
        }

        public Guid Identificador { get; set; }

        public static bool operator ==(EntidadeDominio left, EntidadeDominio right)
        {
            if (object.ReferenceEquals(left, right))
            {
                return true;
            }

            if (((object)left).IsNull() || ((object)right).IsNull())
            {
                return false;
            }

            if (left.Identificador == right.Identificador)
            {
                return true;
            }

            return false;
        }

        public static bool operator !=(EntidadeDominio left, EntidadeDominio right)
        {
            return !(left == right);
        }

        public override bool Equals(object obj)
        {
            return (obj as EntidadeDominio) == this;
        }

        public override int GetHashCode() => unchecked((17 * 23) + this.Identificador.GetHashCode());
    }
}
