﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Dominio.Modelos.Abstracao
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dominio.Modelos.Interfaces;
    using System;

    public abstract class EntidadeDominioAuditavel : EntidadeDominio, IEntidadeAuditavel
    {
        protected EntidadeDominioAuditavel()
        {
            this.DataCriacao = DateTime.UtcNow;
        }

        public DateTime DataCriacao { get; set; }

        public DateTime? DataAlteracao { get; set; }
    }
}
