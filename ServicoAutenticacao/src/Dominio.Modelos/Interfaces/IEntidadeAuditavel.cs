﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Dominio.Modelos.Interfaces
{
    using System;

    public interface IEntidadeAuditavel
    {
        DateTime DataCriacao { get; set; }

        DateTime? DataAlteracao { get; set; }
    }
}
