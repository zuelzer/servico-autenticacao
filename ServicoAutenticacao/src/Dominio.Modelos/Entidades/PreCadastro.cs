﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Dominio.Modelos.Entidades
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dominio.Modelos.Abstracao;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Exceptions;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Extensions;
    using System;

    public class PreCadastro : EntidadeDominioAuditavel
    {
        public PreCadastro(string codigoPais, string numeroIdentificacao, string nome, string email)
        {
            this.CodigoPais = codigoPais;
            this.NumeroIdentificacao = numeroIdentificacao;
            this.Nome = nome;
            this.Email = email;
        }

        public string CodigoPais { get; private set; }

        public string NumeroIdentificacao { get; private set; }

        public string Nome { get; private set; }

        public string NomeSocial { get; private set; }

        public DateTime DataNascimento { get; private set; }

        public string Email { get; private set; }


        public void AtualizarDados(string nome, string nomeSocial, DateTime dataNascimento, string email)
        {
            this.Nome = nome;
            this.NomeSocial = nomeSocial;
            this.DataNascimento = dataNascimento;
            this.Email = email;
        }

        public void Validar()
        {
            Intercept.Against<InvalidDomainEntityException>(this.CodigoPais.IsNull(), "O código do país não pode ser nulo");
            Intercept.Against<InvalidDomainEntityException>(this.CodigoPais.Length > 2, "O código do país não pode ter mais de 2(dois) caracteres");
            Intercept.Against<InvalidDomainEntityException>(this.NumeroIdentificacao.IsNull(), "O número de identificação não pode ser nulo");
            Intercept.Against<InvalidDomainEntityException>(this.NumeroIdentificacao.Length > 20, "O número de identificação não pode ter mais de 20(vinte) caracteres");
            Intercept.Against<InvalidDomainEntityException>(this.Nome.IsNull(), "O nome não pode ser nulo");
            Intercept.Against<InvalidDomainEntityException>(this.Nome.Length > 100, "O nome não pode ter mais de 100(cem) caracteres");
            Intercept.Against<InvalidDomainEntityException>(this.NomeSocial.IsNotNullOrEmpty() && this.NomeSocial.Length > 100, "O nome social não pode ter mais de 100(cem) caracteres");
            Intercept.Against<InvalidDomainEntityException>(this.DataNascimento.IsNull(), "A data de nascimento não pode ser nula");
            Intercept.Against<InvalidDomainEntityException>(this.Email.IsNotNullOrEmpty() && this.Email.Length > 100, "O e-mail não pode ter mais de 100(cem) caracteres");
            // TODO: Implementar
        }
    }
}
