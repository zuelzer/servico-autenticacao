﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Dados.Repositorio.AutoMapeamento
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dados.Repositorio.Entidades;
    using global::AutoMapper;

    public class PerfilMapeamentoDominioParaDados : Profile
    {
        public PerfilMapeamentoDominioParaDados()
        {
            CreateMap<Dominio.Modelos.Entidades.PreCadastro, PreCadastro>();
        }
    }
}
