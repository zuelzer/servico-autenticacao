﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Dados.Repositorio.AutoMapeamento
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dados.Repositorio.Entidades;
    using global::AutoMapper;

    public class PerfilMapeamentoDadosParaDominio : Profile
    {
        public PerfilMapeamentoDadosParaDominio()
        {
            CreateMap<PreCadastro, Dominio.Modelos.Entidades.PreCadastro>();
        }
    }
}
