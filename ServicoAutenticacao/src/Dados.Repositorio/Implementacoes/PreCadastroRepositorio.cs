﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Dados.Repositorio.Implementacoes
{
    using global::AutoMapper;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dados.Repositorio.Contexto;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dados.Repositorio.Entidades;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dominio.Dados.Repositorios;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Threading.Tasks;

    public class PreCadastroRepositorio : IPreCadastroRepositorio
    {
        private readonly ILogger _registrador;
        private readonly IMapper _mapeador;
        private readonly Func<SqlServerContext> _fabricaContexto;

        public PreCadastroRepositorio(ILoggerFactory fabricaRegistrador, IMapper mapeador, Func<SqlServerContext> fabricaContexto)
        {
            this._registrador = fabricaRegistrador.CreateLogger<PreCadastroRepositorio>();
            this._mapeador = mapeador;
            this._fabricaContexto = fabricaContexto;
        }

        public async Task<Dominio.Modelos.Entidades.PreCadastro> Obter(Guid identificador)
        {
            try
            {
                using (var dbContexto = this._fabricaContexto.Invoke())
                {
                    var preCadastroContexto = await dbContexto.PreCadastros.AsNoTracking().FirstOrDefaultAsync(u => u.Identificador == identificador).ConfigureAwait(false);

                    if (preCadastroContexto.IsNotNull())
                    {
                        return this._mapeador.Map<PreCadastro, Dominio.Modelos.Entidades.PreCadastro>(preCadastroContexto);
                    }
                }
            }
            catch (Exception ex)
            {
                _registrador.LogError(ex, $"Não é possível obter uma transação (ObterPreCadastro) do banco de dados: identificador: {identificador}");
            }

            return null;
        }

        public async Task<Dominio.Modelos.Entidades.PreCadastro> ObterPorChaveComposta(string codigoPais, string numeroIdentificacao)
        {
            try
            {
                using (var dbContexto = this._fabricaContexto.Invoke())
                {
                    var preCadastroContexto = await dbContexto.PreCadastros.AsNoTracking().FirstOrDefaultAsync(u => u.CodigoPais == codigoPais && u.NumeroIdentificacao == numeroIdentificacao).ConfigureAwait(false);

                    if (preCadastroContexto.IsNotNull())
                    {
                        return this._mapeador.Map<PreCadastro, Dominio.Modelos.Entidades.PreCadastro>(preCadastroContexto);
                    }
                }
            }
            catch (Exception ex)
            {
                _registrador.LogError(ex, $"Não é possível obter uma transação (ObterPreCadastroPorChaveComposta) do banco de dados: $codigoPais: {codigoPais} - $numeroIdentificacao: {numeroIdentificacao}");
            }

            return null;
        }

        public async Task<Dominio.Modelos.Entidades.PreCadastro> Inserir(Dominio.Modelos.Entidades.PreCadastro preCadastro)
        {
            try
            {
                using (var dbContexto = this._fabricaContexto.Invoke())
                {
                    var preCadastroContexto = this._mapeador.Map<Dominio.Modelos.Entidades.PreCadastro, PreCadastro>(preCadastro);

                    dbContexto.Add(preCadastroContexto);

                    await dbContexto.SaveChangesAsync().ConfigureAwait(false);

                    return this._mapeador.Map<PreCadastro, Dominio.Modelos.Entidades.PreCadastro>(preCadastroContexto);
                }
            }
            catch (Exception ex)
            {
                _registrador.LogError(ex, $"Não é possível salvar uma transação (InserirPreCadastro) no banco de dados: $identificador: {preCadastro.Identificador} - $numeroIdentificacao: {preCadastro.NumeroIdentificacao} - $nome: {preCadastro.Nome}");
            }

            return null;
        }

        public async Task<Dominio.Modelos.Entidades.PreCadastro> Atualizar(Dominio.Modelos.Entidades.PreCadastro preCadastro)
        {
            try
            {
                using (var dbContexto = this._fabricaContexto.Invoke())
                {
                    var preCadastroContexto = this._mapeador.Map<Dominio.Modelos.Entidades.PreCadastro, PreCadastro>(preCadastro);

                    dbContexto.Update(preCadastroContexto);

                    await dbContexto.SaveChangesAsync().ConfigureAwait(false);

                    return this._mapeador.Map<PreCadastro, Dominio.Modelos.Entidades.PreCadastro>(preCadastroContexto);
                }
            }
            catch (Exception ex)
            {
                _registrador.LogError(ex, $"Não é possível salvar uma transação (AtualizarPreCadastro) no banco de dados: $identificador: {preCadastro.Identificador} - $numeroIdentificacao: {preCadastro.NumeroIdentificacao} - $nome: {preCadastro.Nome}");
            }

            return null;
        }

        public async Task Deletar(Dominio.Modelos.Entidades.PreCadastro preCadastro)
        {
            try
            {
                using (var dbContexto = this._fabricaContexto.Invoke())
                {
                    var preCadastroContexto = this._mapeador.Map<Dominio.Modelos.Entidades.PreCadastro, PreCadastro>(preCadastro);

                    dbContexto.Remove(preCadastroContexto);

                    await dbContexto.SaveChangesAsync().ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                _registrador.LogError(ex, $"Não é possível deletar uma transação (DeletarPreCadastro) no banco de dados: $identificador: {preCadastro.Identificador} - $numeroIdentificacao: {preCadastro.NumeroIdentificacao} - $nome: {preCadastro.Nome}");
            }
        }
    }
}
