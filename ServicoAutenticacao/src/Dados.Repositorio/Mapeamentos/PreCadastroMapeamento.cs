﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Dados.Repositorio.Mapeamentos
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dados.Repositorio.Entidades;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class MapeamentoPreCadastro : IEntityTypeConfiguration<PreCadastro>
    {
        public void Configure(EntityTypeBuilder<PreCadastro> builder)
        {
            builder.HasKey(o => o.Identificador);

            builder.Property(o => o.CodigoPais).HasColumnName("CodigoPais").HasColumnType("char").HasMaxLength(2).IsRequired();
            builder.Property(o => o.NumeroIdentificacao).HasColumnName("NumeroIdentificacao").HasColumnType("varchar").HasMaxLength(20).IsRequired();
            builder.Property(o => o.Nome).HasColumnName("Nome").HasColumnType("varchar").HasMaxLength(100).IsRequired();
            builder.Property(o => o.NomeSocial).HasColumnName("NomeSocial").HasColumnType("varchar").HasMaxLength(100);
            builder.Property(o => o.DataNascimento).HasColumnName("DataNascimento").HasColumnType("datetime").IsRequired();
            builder.Property(o => o.Email).HasColumnName("Email").HasColumnType("varchar").HasMaxLength(100);
            
            builder.Property(o => o.DataCriacao).HasColumnName("DataCriacao").HasColumnType("datetime").IsRequired();
            builder.Property(o => o.DataAlteracao).HasColumnName("DataAlteracao").HasColumnType("datetime");

            builder.ToTable("PreCadastro");
        }
    }
}
