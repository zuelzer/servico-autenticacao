﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Dados.Repositorio.Contexto
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dados.Repositorio.Entidades;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dados.Repositorio.Mapeamentos;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Configuration;
    using Microsoft.EntityFrameworkCore;

    public class SqlServerContext : DbContext
    {
        private readonly EntityFrameworkConfiguration _entityFrameworkConfiguration;

        public SqlServerContext(EntityFrameworkConfiguration entityFrameworkConfiguration)
            : base()
        {
            this._entityFrameworkConfiguration = entityFrameworkConfiguration;
        }

        public DbSet<PreCadastro> PreCadastros { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            this.AddEntityConfigurations(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(this._entityFrameworkConfiguration.ToString());
            }
        }

        private void AddEntityConfigurations(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new MapeamentoPreCadastro());
        }
    }
}
