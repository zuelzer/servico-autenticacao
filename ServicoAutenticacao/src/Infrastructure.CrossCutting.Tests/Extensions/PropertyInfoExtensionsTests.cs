﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Extensions.Tests
{
    using Xunit;

    [Trait("Category", "Infrastructure.CrossCutting")]
    public class PropertyInfoExtensionsTests
    {
        [Fact]
        public void FastGetValue_ReadProperty_ReturnsValue()
        {
            // Arrange
            var obj = new { Property = "Teste" };

            // Act
            var value = obj.GetType().GetProperty("Property").FastGetValue(obj);

            // Assert
            Assert.Equal(obj.Property, value);
        }
    }
}
