﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Extensions.Tests
{
    using Xunit;

    [Trait("Category", "Infrastructure.CrossCutting")]
    public class StringExtensionsTests
    {
        [Fact]
        public void IsNumber_NotNumberString_ReturnsFalse()
        {
            // Arrange
            string str = "teste";

            // Act
            var result = str.IsNumber();

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void IsNumber_NotNumberString_ReturnsTrue()
        {
            // Arrange
            string str = "123";

            // Act
            var result = str.IsNumber();

            // Assert
            Assert.True(result);
        }
        [Fact]
        public void IsNumberWithWhiteSpace_NumberWithoutWhiteSpaceString_ReturnsFalse()
        {
            // Arrange
            string str = "1234-5678-1234-5678";

            // Act
            var result = str.IsNumberWithWhiteSpace();

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void IsNumberWithWhiteSpace_NumberWithWhiteSpaceString_ReturnsTrue()
        {
            // Arrange
            string str = "1234 5678 1234 5678";

            // Act
            var result = str.IsNumberWithWhiteSpace();

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void IsNullOrEmpty_NotNullOrEmptyString_ReturnsFalse()
        {
            // Arrange
            string str = "teste";

            // Act
            var result = str.IsNullOrEmpty();

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void IsNullOrEmpty_EmptyString_ReturnsTrue()
        {
            // Arrange
            string str = string.Empty;

            // Act
            var result = str.IsNullOrEmpty();

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void IsNotNullOrEmpty_NullString_ReturnsFalse()
        {
            // Arrange
            string str = null;

            // Act
            var result = str.IsNotNullOrEmpty();

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void IsNotNullOrEmpt_EmptyString_ReturnsFalse()
        {
            // Arrange
            string str = string.Empty;

            // Act
            var result = str.IsNotNullOrEmpty();

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void IsNullOrEmpty_NullString_ReturnsTrue()
        {
            // Arrange
            string str = null;

            // Act
            var result = str.IsNullOrEmpty();

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void IsNullOrWhiteSpace_NotNullOrWhiteSpaceString_ReturnsFalse()
        {
            // Arrange
            string str = "teste";

            // Act
            var result = str.IsNullOrWhiteSpace();

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void IsNullOrWhiteSpace_WhiteSpaceString_ReturnsTrue()
        {
            // Arrange
            string str = " ";

            // Act
            var result = str.IsNullOrWhiteSpace();

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void IsNullOrWhiteSpace_NullString_ReturnsTrue()
        {
            // Arrange
            string str = null;

            // Act
            var result = str.IsNullOrWhiteSpace();

            // Assert
            Assert.True(result);
        }
    }
}
