﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Tests.Extensions
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Extensions;
    using Xunit;

    [Trait("Category", "Infrastructure.CrossCutting")]
    public class ConvertExtensionsTests
    {
        private enum FakeEnumFrom
        {
            One = 1,
            Two = 2
        }

        private enum FakeEnumTo
        {
            One = 1,
            Two = 2
        }

        [Fact]
        public void To_NullObjectToNullable_ReturnsNull()
        {
            object obj = null;

            int? convertedObj = obj.To<int?>();

            Assert.Null(convertedObj);
        }

        [Fact]
        public void To_NullObjectToNonNullable_ReturnsNull()
        {
            object obj = null;

            string convertedObj = obj.To<string>();

            Assert.Null(convertedObj);
        }

        [Fact]
        public void To_ObjectToNullable_ReturnsConvertedValue()
        {
            object expectedObj = 10;

            int? convertedObj = expectedObj.To<int?>();

            Assert.Equal(typeof(int), convertedObj.GetType());
            Assert.Equal(expectedObj, convertedObj.Value);
        }

        [Fact]
        public void To_ObjectToNonNullable_ReturnsConvertedValue()
        {
            object expectedObj = "teste";

            string convertedObj = expectedObj.To<string>();

            Assert.Equal(typeof(string), convertedObj.GetType());
            Assert.Equal(expectedObj, convertedObj);
        }

        [Fact]
        public void To_EnumObject_ReturnsConvertedValue()
        {
            FakeEnumFrom obj = FakeEnumFrom.One;

            FakeEnumTo convertedObj = obj.To<FakeEnumTo>();

            Assert.Equal(typeof(FakeEnumTo), convertedObj.GetType());
        }
    }
}
