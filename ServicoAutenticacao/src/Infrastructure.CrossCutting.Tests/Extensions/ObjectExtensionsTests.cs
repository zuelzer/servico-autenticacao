﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Extensions.Tests
{
    using Xunit;

    [Trait("Category", "Infrastructure.CrossCutting")]
    public class ObjectExtensionsTests
    {
        [Fact]
        public void IsNull_NullObject_ReturnTrue()
        {
            // Arrange
            object nullObj = null;

            // Act
            var result = nullObj.IsNull();

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void IsNull_NotNullObject_ReturnTrue()
        {
            // Arrange
            var notNullObj = new { StringProp = "teste" };

            // Act
            var result = notNullObj.IsNull();

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void IsNotNull_NullObject_ReturnTrue()
        {
            // Arrange
            object nullObj = null;

            // Act
            var result = nullObj.IsNotNull();

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void IsNotNull_NotNullObject_ReturnTrue()
        {
            // Arrange
            var notNullObj = new { StringProp = "teste" };

            // Act
            var result = notNullObj.IsNotNull();

            // Assert
            Assert.True(result);
        }
    }
}
