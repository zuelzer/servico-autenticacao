﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Exceptions.Tests
{
    using Newtonsoft.Json;
    using System;
    using Xunit;

    [Trait("Category", "Infrastructure.CrossCutting")]
    public class InterceptTests
    {
        [Fact]
        public void Against_AssertionTrueAndMessage_ThrowsArgumentExceptionWithMessage()
        {
            try
            {
                // Act
                Intercept.Against<ArgumentException>(true, "Message");
            }
            catch (ArgumentException ex)
            {
                // Assert
                Assert.Equal("Message", ex.Message);
                Assert.ThrowsAsync<ArgumentException>(() => throw ex);
            }
        }

        [Fact]
        public void Against_AssertionFalseAndMessage_DoesntThrowException()
        {
            // Act
            Intercept.Against<ArgumentException>(false, "Message");
        }

        [Fact]
        public void Against_AssertionTrueWithAditionalInfo_ThrowsArgumentExceptionWithAditionalInfo()
        {
            // Arrange
            var logData = new { zero = "zero", one = "one" };
            var serializedLogData = JsonConvert.SerializeObject(logData);

            try
            {
                // Act
                Intercept.Against<ArgumentException>(true, "Message", logData);
            }
            catch (ArgumentException ex)
            {
                // Assert
                Assert.True(ex.Data.Contains("LogData"));
                Assert.Equal(serializedLogData, ex.Data["LogData"]);
                Assert.ThrowsAsync<ArgumentException>(() => throw ex);
            }
        }

        [Fact]
        public void Against_AssertionTrueWithInnerException_ThrowsArgumentExceptionWithInnerException()
        {
            // Arrange
            var innerException = new Exception();

            try
            {
                // Act
                Intercept.Against<ArgumentException>(true, "Message", innerException);
            }
            catch (ArgumentException ex)
            {
                // Assert
                Assert.Equal("Message", ex.Message);
                Assert.Same(innerException, ex.InnerException);
                Assert.ThrowsAsync<ArgumentException>(() => throw ex);
            }
        }

        [Fact]
        public void Against_AssertionFalseWithInnerException_DoesntThrowException()
        {
            // Act
            Intercept.Against<ArgumentException>(false, "Message", new Exception());
        }

        [Fact]
        public void Against_AssertionTrueWithTypeAsParameter_ThrowsArgumentException()
        {
            // Arrange
            var innerException = new Exception();
            var logData = new { test = "123" };
            var serializedLogData = JsonConvert.SerializeObject(logData);

            try
            {
                // Act
                Intercept.Against(typeof(ArgumentException), true, "Error Message", logData, innerException);
            }
            catch (ArgumentException ex)
            {
                // Assert
                Assert.Equal("Error Message", ex.Message);
                Assert.True(ex.Data.Contains("LogData"));
                Assert.Equal(serializedLogData, ex.Data["LogData"]); Assert.Same(innerException, ex.InnerException);
                Assert.ThrowsAsync<ArgumentException>(() => throw ex);
            }
        }
    }
}
