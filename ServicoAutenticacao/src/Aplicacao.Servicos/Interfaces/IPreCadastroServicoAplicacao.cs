﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Aplicacao.Servicos.Interfaces
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Aplicacao.DTO.Resposta;
    using System;
    using System.Threading.Tasks;

    public interface IPreCadastroServicoAplicacao
    {
        Task<PreCadastro> ObterPreCadastro(Guid identificador);

        Task<PreCadastro> InserirPreCadastro(DTO.Solicitacao.PreCadastro preCadastro);

        Task<PreCadastro> AtualizarPreCadastro(Guid identificador, DTO.Solicitacao.PreCadastro preCadastro);

        Task DeletarPreCadastro(Guid identificador);
    }
}
