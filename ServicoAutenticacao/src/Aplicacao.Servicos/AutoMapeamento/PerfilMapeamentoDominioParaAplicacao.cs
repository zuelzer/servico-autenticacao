﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Aplicacao.Servicos.AutoMapeamento
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Aplicacao.DTO.Resposta;
    using global::AutoMapper;

    public class PerfilMapeamentoDominioParaAplicacao : Profile
    {
        public PerfilMapeamentoDominioParaAplicacao()
        {
            CreateMap<Dominio.Modelos.Entidades.PreCadastro, PreCadastro>();
        }
    }
}
