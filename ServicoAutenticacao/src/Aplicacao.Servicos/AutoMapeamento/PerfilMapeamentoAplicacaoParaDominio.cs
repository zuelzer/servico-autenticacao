﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Aplicacao.Servicos.AutoMapeamento
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Aplicacao.DTO.Solicitacao;
    using global::AutoMapper;

    public class PerfilMapeamentoAplicacaoParaDominio : Profile
    {
        public PerfilMapeamentoAplicacaoParaDominio()
        {
            CreateMap<PreCadastro, Dominio.Modelos.Entidades.PreCadastro>();
        }
    }
}
