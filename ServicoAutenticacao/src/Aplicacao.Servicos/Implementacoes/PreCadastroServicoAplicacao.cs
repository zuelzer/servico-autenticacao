﻿namespace Employer.Plataforma.Interno.ServiceAutenticacao.Aplicacao.Servicos.Implementacoes
{
    using Employer.Plataforma.Interno.ServiceAutenticacao.Aplicacao.DTO.Resposta;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Aplicacao.Servicos.Interfaces;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Dominio.Servicos.Interfaces;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Exceptions;
    using Employer.Plataforma.Interno.ServiceAutenticacao.Infrastructure.CrossCutting.Extensions;
    using global::AutoMapper;
    using System;
    using System.Threading.Tasks;

    public class PreCadastroServicoAplicacao : IPreCadastroServicoAplicacao
    {
        private readonly IMapper _mapeador;
        private readonly IPreCadastroServico _preCadastroServico;

        public PreCadastroServicoAplicacao(IMapper mapeador, IPreCadastroServico preCadastroServico)
        {
            this._mapeador = mapeador;
            this._preCadastroServico = preCadastroServico;
        }

        public async Task<DTO.Resposta.PreCadastro> ObterPreCadastro(Guid identificador)
        {
            var preCadastroDominio = await this._preCadastroServico.ObterPreCadastro(identificador).ConfigureAwait(false);

            Intercept.Against<ResourceNotFoundException>(preCadastroDominio.IsNull(), "Pré cadastro não encontrado");

            return this._mapeador.Map<Dominio.Modelos.Entidades.PreCadastro, PreCadastro>(preCadastroDominio);
        }

        public async Task<DTO.Resposta.PreCadastro> InserirPreCadastro(DTO.Solicitacao.PreCadastro preCadastro)
        {
            Intercept.Against<InvalidDomainEntityException>(preCadastro.IsNull(), "Pré cadastro não pode ser nulo");

            var preCadastroDominio = this._mapeador.Map<DTO.Solicitacao.PreCadastro, Dominio.Modelos.Entidades.PreCadastro>(preCadastro);

            preCadastroDominio = await this._preCadastroServico.InserirPreCadastro(preCadastroDominio).ConfigureAwait(false);

            return this._mapeador.Map<Dominio.Modelos.Entidades.PreCadastro, PreCadastro>(preCadastroDominio);
        }

        public async Task<DTO.Resposta.PreCadastro> AtualizarPreCadastro(Guid identificador, DTO.Solicitacao.PreCadastro preCadastro)
        {
            Intercept.Against<InvalidDomainEntityException>(preCadastro.IsNull(), "Pré cadastro não pode ser nulo");

            var preCadastroDominio = await this._preCadastroServico.ObterPreCadastro(identificador).ConfigureAwait(false);

            if (preCadastroDominio.IsNotNull())
            {
                preCadastroDominio.AtualizarDados(preCadastro.Nome, preCadastro.NomeSocial, preCadastro.DataNascimento, preCadastro.Email);
            }

            preCadastroDominio = await this._preCadastroServico.AtualizarPreCadastro(preCadastroDominio).ConfigureAwait(false);

            return this._mapeador.Map<Dominio.Modelos.Entidades.PreCadastro, PreCadastro>(preCadastroDominio);
        }

        public async Task DeletarPreCadastro(Guid identificador)
        {
            await this._preCadastroServico.DeletarPreCadastro(identificador).ConfigureAwait(false);
        }
    }
}
